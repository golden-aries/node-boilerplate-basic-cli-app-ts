import { ILogger, ISettings, LogLevel } from "../common/abstractions";

export class ConsoleLogger implements ILogger {

    private _settings : ISettings;

    constructor(settings: ISettings){
        this._settings = settings;
    }
    log(logLevel: LogLevel, message?: any, ...optionalParams: any[]) {
        if (this._settings.logLevel <= logLevel)
        {
            switch(logLevel)
            {
                case LogLevel.Error:
                    console.error("%s%s: " + message, this._settings.logPrefix, LogLevel[logLevel], ...optionalParams)
                    break;
                default:
                    console.log("%s%s: " + message, this._settings.logPrefix, LogLevel[logLevel], ...optionalParams);
                    break;
            }
        }
    };

    logDebug(message?: any, ...optionalParams: any[]) {
        this.log(LogLevel.Debug, message, ...optionalParams);
    }
    logWarning(message?: any, ...optionalParams: any[]){
        this.log(LogLevel.Warning, message, ...optionalParams);
    }

    logCritical(message?: any, ...optionalParams: any[]) {
        this.log(LogLevel.Warning, message, ...optionalParams);
    }

    logInformation(message?: any, ...optionalParams: any[]) {
        this.log(LogLevel.Information, message, ...optionalParams);
    }

    logError(message?: any, ...optionalParams: any[]) {
        this.log(LogLevel.Error, message, ...optionalParams);
    }

    logTrace(message?: any, ...optionalParams: any[]) {
        this.log(LogLevel.Trace, message, ...optionalParams);
    }
}