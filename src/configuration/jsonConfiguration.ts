import fs from 'fs/promises';
import { isString } from 'lodash';
import merge from 'lodash/merge';
import { ISettings, LogLevel } from '../common/abstractions';
import { UnableToLoadConfiguration, UnknownError } from '../common/errors';
import { IConfiguration } from './abstractions';

export class JsonConfiguration implements IConfiguration {
    /**
     * @param path path to the json configuration file to load
     * @param defaultSettings loaded configuration will be merged with default configuration
     * @param optional default: false. If true will return defaultConfiguration instead of throwing exception on failure to load configuration from json file
     */
    constructor(
        private path: string,
        private defaultSetting: ISettings,
        private optional?: boolean) { }

    async getSettings(): Promise<ISettings> {
        return this._getSettings();
    }

    private async _getSettings(): Promise<ISettings> {
        try {
            const fileName = this.path;
            const content = await fs.readFile(fileName);
            const obj = JSON.parse(content.toString());
            if (isString(obj.logLevel)) {
                obj.logLevel = LogLevel[obj.logLevel];
                if (obj.logLevel === undefined) {
                    throw new UnknownError('Unknown LogLevel!');
                }
            }
            const result = merge<ISettings, ISettings>(this.defaultSetting, obj);
            return result;
        } catch (err) {
            if (!this.optional) {
                throw new UnableToLoadConfiguration(err, 'Unable to load configuration from json file!', { fileName: this.path });
            }
            else {
                return this.defaultSetting;
            }
        }
    }
}