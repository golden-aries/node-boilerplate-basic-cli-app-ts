import { ISettings } from '../common/abstractions';

export interface IConfiguration {
  getSettings(): Promise<ISettings>;
}