import path from "path";
import { ILogger, ISettings, LogLevel } from "../common/abstractions";
import * as os from "os"
import { IConfiguration } from "../configuration/abstractions";
import { JsonConfiguration } from "../configuration/jsonConfiguration";

export let settings: ISettings;
export let logger: ILogger;

function defaultSettings(): ISettings {
    return {
        baseUrl: 'http://localhost',
        apiKey: '',
        logLevel: LogLevel.Warning,
        logPrefix: "MyLog#"
    };
}

/** returns default (hard coded) storage file name */
function defaultConfigurationFileName(): string {
    const result = path.join(os.homedir(), '.MyPackageConfigFolder', 'MyPackageConfig.json');
    return result;
}

/** returns storage file name from the settings or default one*/
function getConfigurationFileName(): string {
    return process.env.MyFancyConfig || defaultConfigurationFileName();
}

let configuration: IConfiguration | undefined = undefined;
export function getConfiguration(): IConfiguration {
  return configuration ?? 
  (configuration = new JsonConfiguration(getConfigurationFileName(), defaultSettings(), true));
}

export async function initSettings(): Promise<ISettings> {
    const finalSettings = settings ?? (settings = await getConfiguration().getSettings());
    return finalSettings;
  }

export async function initServices() {
    await initSettings();
}