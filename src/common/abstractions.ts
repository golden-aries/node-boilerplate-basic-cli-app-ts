export interface ISettings {
    baseUrl: string;
    apiKey: string;
    logLevel: LogLevel;
    logPrefix: string;
  }

export enum LogLevel {
    Trace = 0,
    Debug = 1,
    Information = 2,
    Warning = 3,
    Error = 4,
    Critical = 5,
    None = 6,
  }
  
  export interface ILogger {
    log: (logLevel: LogLevel, message?: any, ...optionalParams: any[]) => void;
    logInformation: (message?: any, ...optionalParams: any[]) => void;
    logError: (message?: any, ...optionalParams: any[]) => void;
    logTrace: (message?: any, ...optionalParams: any[]) => void;
    logDebug: (message?: any, ...optionalParams: any[]) => void;
    logWarning: (message?: any, ...optionalParams: any[]) => void;
    logCritical: (message?: any, ...optionalParams: any[]) => void;
  }