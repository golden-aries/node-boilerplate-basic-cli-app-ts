export class BaseError extends Error {

    /**
     *
     */
    constructor(
        public exceptionId: string,
        message: string,
        public innerError?: unknown,
        public data?: any) {
        super(message ?? exceptionId);

    }

    static PathIsNotAFile: string = "Path is not a file!";
    static PathNotADirectory: string = "Path is not a directory!";
    static NotIplemented: string = "Not Implemented!";
    static NotAClass: string = "Not a Class";
    static UnknownError: string = "Unknown Error Encountered";
    static UnableToLoadConfiguration: string = 'Unable to load configuration!';
}

export class UnknownError extends BaseError {
    /**
     *
     */
    constructor(originalError: unknown, message?: string) {
        super(BaseError.UnknownError,
            message ?? BaseError.UnknownError,
            originalError);
    }
}

export class NotImplementedError extends BaseError {
    /**
     *
     */
    constructor(message?: string) {
        super(BaseError.NotIplemented,
            message ?? BaseError.NotIplemented);
    }
}

export class UnableToLoadConfiguration extends BaseError {
    /**
     *
     */
    constructor(originalError?: unknown, message?: string, data?: any) {
        super(BaseError.UnableToLoadConfiguration, message ?? BaseError.UnableToLoadConfiguration, originalError, data);
    }
}