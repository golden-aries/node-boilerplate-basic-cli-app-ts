import * as readline from "readline";
import { LogLevel } from "./common/abstractions";
import { initSettings, settings } from "./services/serviceProvider";

const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
})

rl.on("close", function() {
    console.log("\nBYE BYE !!!");
    process.exit(0);
});

(async  function run(){
    await initSettings();
    console.log(`Configured  LogLevel is ${LogLevel[settings.logLevel]}`)
    console.log("Hello World!");
    // rl.question("Hit enter key to exit ... ", function(name) {
    //     console.log(`Exiting`);
    //     rl.close();
    // });
    rl.close();
})();

